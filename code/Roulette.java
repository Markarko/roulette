import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
public class Roulette {
    public static void main(String[] args){
        RouletteWheel rw = new RouletteWheel();
        Scanner s = new Scanner(System.in);
        List<String> betOptions = Arrays.asList("n", "h", "l", "r", "b", "e", "o");
        char quit = 'n';
        System.out.println("How much money do you have?");
        int money = s.nextInt();
        //s.nextLine();
        do{
            
            System.out.println("How much would you like to bet?");
            int betAmount = s.nextInt();
            String betOption;
            do{
                displayBetOptions(betOptions);
                betOption = s.next();
            } while (!validOption(betOptions, betOption));
            int number = s.nextInt();
            rw.spin();
            boolean won = number == rw.getValue();
            money += won ? betAmount *35 : -betAmount;
            System.out.println("You" + (won ? " won" : " lost") + ". You have now " + money + "$");
            System.out.println("Would you like to quit? (y/n)");
            quit = s.next().charAt(0);
            
        } while (quit != 'y');  
    }
    public static void displayBetOptions(List<String> betOptions){
        System.out.println("Please choose on what you want to bet:");
        System.out.println(betOptions.get(0) + " - bet on a number (0 - 36)");
        System.out.println(betOptions.get(1) + " - bet on a high number");
        System.out.println(betOptions.get(2) + " - bet on a low number");
        System.out.println(betOptions.get(3) + " - bet on a red number");
        System.out.println(betOptions.get(4) + " - bet on a black number");
        System.out.println(betOptions.get(5) + " - bet on an even number");
        System.out.println(betOptions.get(6) + " - bet on an odd number");
    }
    public static boolean validOption(List<String> betOptions, String option){
        return betOptions.contains(option);
    }
}

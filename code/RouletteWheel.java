import java.util.Random;
public class RouletteWheel{
    private Random random;
    private int number = 0;
    public RouletteWheel(){
        this.random =  new Random();
    }
    public void spin(){
        this.number = this.random.nextInt(37);
    }
    public int getValue(){
        return this.number;
    }
    public boolean isLow(){
        return this.number >= 1 && this.number <= 18;
    }
    public boolean isHigh(){
        return this.number >= 19 && this.number <= 36;
    }
    public boolean isEven(){
        return !isZero() && this.number % 2 == 0;
    }
    public boolean isOdd(){
        return !isZero() && !isEven();
    }
    public boolean isRed(){
        return this.number >= 1 && this.number <= 10 && isEven() ||
               this.number >= 19 && this.number <= 28 && isEven();
    }
    public boolean isBlack(){
        return !isRed() && !isZero();
    }
    public boolean isZero(){
        return this.number == 0;
    }
}